const Page = require('./page');

class StorePage extends Page {
    // define url path to open before each test
    open () {
        return browser.url('/store');
    }
    // define selectors using getter methods
    get boltsNFT () {
        return $('p=Bolts');
    }

    get townFilter () {
        return $('p=Town Star');
    }

    get epicScrollView () {
        return $('p=Epic');
    }

    get epicFilter () {
        return $$('.option-filter:nth-child(4) div[data-testid="item-filter-checkbox"]')[0];
    }

    get spiderFilter () {
        return $('p=Spider Tanks');
    }

    get rareScrollView () {
        return $('p=Rare');
    }

    get rareFilter () {
        return $('[data-testid="store-expansion-panel-header"] + div .option-filter:nth-child(3) [data-testid="item-filter-checkbox"]');
    }

    get searchInput () {
        return $('[data-testid="search-bar-input"]');
    }
    // function that passes through given search value
    async search (NFTValue) {
        await this.searchInput.setValue(NFTValue);
    }
};

module.exports = new StorePage();