const Page = require('./page');

class LoginPage extends Page {
    // define selectors using getter methods
    get loginBtn () {
        return $('button=Login');
    }

    get dropDownBtn () {
        return $('#main-dropdown-arrow');
    }

    get acctBtn () {
        return $('div=Account');
    }

    get inputUsername () {
        return $('#username');
    }

    get inputPassword () {
        return $('#password');
    }

    get btnSubmit () {
        return $('button[type="submit"]');
    }

    // login using username and password
    async login (username, password) {
        await this.inputUsername.setValue(username);
        await this.inputPassword.setValue(password);
        await this.btnSubmit.click();
    }

    open () {
        return super.open('login');
    }
};

module.exports = new LoginPage();