const Page = require('./page');

class GamesPage extends Page {
    // define path to open before each test
    open () {
        return browser.url('/games');
    }
    // define selectors using getter methods
    get townBtn () {
         return $('button=Play');
    }
 
    get newUsrForm () {
        return $('h2=Create your Account');
    }
};

module.exports = new GamesPage();