module.exports = class Page {
    open (path) {
        return browser.url(`https://app.gala.games/${path}`);
    }

    open2 () {
        return browser.url(`https://stage-games.gala.com/`);
    }

    get acceptAllBtn () {
        return $('>>>[data-testid="uc-accept-all-button"]');
    }
};