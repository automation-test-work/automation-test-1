const { describe } = require('mocha');
const GamesPage = require('../pageobjects/games.page');

describe('Town Star Launch', async () => {
    it('Should verify Townstar does not launch without being logged in', async () => {
        await GamesPage.open();
        
        await GamesPage.acceptAllBtn.click();
        await GamesPage.townBtn.scrollIntoView({ block: 'center', inline: 'center' });
        await GamesPage.townBtn.click();

        await expect(GamesPage.newUsrForm).toBeDisplayed();
    });
});