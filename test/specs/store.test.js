const { describe } = require('mocha');
const StorePage = require('../pageobjects/store.page');

beforeEach(async function () {
    await StorePage.open();
});

describe('Search using search bar', () => {
    it('Should Search for the Bolts NFT', async () => {

        await StorePage.acceptAllBtn.click();
        await StorePage.search('Bolts');

        await expect(StorePage.boltsNFT).toBeDisplayed();
    });
});

describe('Search using filters, Town Star', () => {
    it('Should filter Town Star items by Epic Rarity', async () => {

            await StorePage.townFilter.scrollIntoView({ block: 'center', inline: 'center' });
            await StorePage.townFilter.click();
            await StorePage.epicScrollView.scrollIntoView({ block: 'center', inline: 'center' });
            await StorePage.epicFilter.waitForClickable();
            await StorePage.epicFilter.click();

            await expect(browser).toHaveUrlContaining('town-star' && 'epic');
    });
});

describe('Search using filters, Spider Tanks', () => {
    it('Should filter Spider Tanks items by Rare Rarity', async () => {

        await StorePage.spiderFilter.scrollIntoView({ block: 'center', inline: 'center' });
        await StorePage.spiderFilter.click();
        await StorePage.rareScrollView.scrollIntoView({ block: 'center', inline: 'center' });
        await StorePage.rareFilter.waitForClickable();
        await StorePage.rareFilter.click();

        await expect(browser).toHaveUrlContaining('spider-tanks' && 'rare');
    });
});