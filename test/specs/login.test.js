const { describe } = require('mocha');
const LoginPage = require('../pageobjects/login.page')

describe('Logging into Games', () => {
    it('Should log into Gala Games and navigate to Account settings page', async () => {
        await LoginPage.open2();

        await LoginPage.acceptAllBtn.click();
        await LoginPage.loginBtn.click();
        await LoginPage.login('arrigoryan13@gmail.com', 'Abc123$&@');
        await LoginPage.dropDownBtn.click();
        await LoginPage.acctBtn.click();

        await expect(browser).toHaveUrlContaining('account');
    });
});